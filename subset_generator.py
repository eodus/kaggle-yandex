#!/usr/bin/env python
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

import sqlite3
import os
import sys
import random
import getopt


def randomUserIdList(cursor, part_size):
    random.seed(1)
    cursor.execute("select distinct `user_id` from `session`")
    user_ids = [x[0] for x in cursor]
    part_size = min(part_size, len(user_ids))
    return random.sample(user_ids, part_size)

def generateSubsequentDB(subsequent_db_name, c, part_size):
    c.execute("attach database \"%s\" as `subset`" % (subsequent_db_name,))

    lst = ",".join(str(x) for x in randomUserIdList(c, part_size))

    c.execute("create table subset.`session` as \
                select * from `session` where `user_id` in (%s)" % (lst,))

    c.execute("create table subset.`click` as \
                select * from `click` where `session_id` in (select `id` from subset.`session`)")

    c.execute("create table subset.`query_action` as \
                select * from `query_action` where `session_id` in (select `id` from subset.`session`)")

    c.execute("create table subset.`query_action_url` as \
                select * from `query_action_url` where `query_action_id` in (select `id` from subset.`query_action`)")

    c.execute("create table subset.`url` as \
                select * from `url` where `id` in (select `url_id` from `query_action_url`)")

    c.execute("create table subset.`term_query` as \
                select * from `term_query` where `query_id` in (select `query_id` from subset.`query_action`)")


if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:], "i:o:c:v", ["ifile=", "ofile=", "usercount=", "verbose"])
    except getopt.GetoptError as err:
        print str(err)
        print "Usage: " + sys.argv[0] +' -i <inputfile> -o <outputfile> -c <usercount>'
        sys.exit(2)

    inputfile = "./train.sqlite"
    outputfile = ""
    usercount = 1000
    verbose = False
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print "Usage: " + sys.argv[0] +' -i <inputfile> -o <outputfile> -c <usercount>'
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        elif opt in ("-c", "--usercount"):
            usercount = int(arg)
        elif opt in ("-v", "--verbose"):
            verbose = True
        else:
            assert False, "unhandled option"

    if outputfile == "":
        outputfile = "subset." + inputfile

    if not os.path.exists(inputfile):
        print "Input file '%s' does not exist!'" % inputfile
        sys.exit(2)

    if os.path.exists(outputfile):
        os.remove(outputfile)
    sqlite3.connect(outputfile).close()

    conn = sqlite3.connect(inputfile)
    c = conn.cursor()

    generateSubsequentDB(outputfile, c, usercount)

    conn.commit()
    conn.close()
