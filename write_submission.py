#!/usr/bin/env python
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

import sqlite3

def writeSubmission(c, output_file):
    c.execute("select session.id, query_action_url.url_id from session \
               inner join query_action on session.id = query_action.session_id and query_action.is_test \
               inner join query_action_url on query_action.id = query_action_url.query_action_id \
               order by session.id, query_action_url.number_in_search_result")
    output_file.write("SessionID,URLID")
    for line in c:
        output_file.write("\n{0},{1}".format(str(line[0]), str(line[1])))

#if __name__ == "__main__":
#    conn = sqlite3.connect("data/dataset.db")
#    c = conn.cursor()
#    f = open("out", "w")
#    writeSubmission(c, f)
#    conn.close()
#    f.close()
