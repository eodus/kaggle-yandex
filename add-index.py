#!/usr/bin/env python
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

import sqlite3
import os
import gzip
import sys
import getopt
import re


if __name__ == "__main__":
    dbfile = sys.argv[1]

    if not os.path.exists(dbfile):
        print "Input file '%s' does not exist!'" % dbfile
        sys.exit(2)

    conn = sqlite3.connect(dbfile, isolation_level = "EXCLUSIVE")

    c = conn.cursor()
    c.execute("create index if not exists `session_day_index` on session(day)")
    c.execute("create index if not exists `session_user_id_index` on session(user_id)")
    conn.commit()
    print "TABLE `session` indexed"

    c.execute("create index if not exists `url_domain_id_index` on url(domain_id)")
    conn.commit()
    print "TABLE `url` indexed"

    c.execute("create index if not exists `query_action_session_id_index` on query_action(session_id)")
    c.execute("create index if not exists `query_action_serpid_index` on query_action(serpid)")
    c.execute("create index if not exists `query_action_query_id_index` on query_action(query_id)")
    c.execute("create index if not exists `query_action_time_passed_index` on query_action(time_passed)")
    conn.commit()
    print "TABLE `query_action` indexed"

    c.execute("create index if not exists `query_action_url_query_action_id_index` on query_action_url(query_action_id)")
    c.execute("create index if not exists `query_action_url_url_id_index` on query_action_url(url_id)")
    c.execute("create index if not exists `query_action_url_number_in_search_result_index` on query_action_url(number_in_search_result)")
    conn.commit()
    print "TABLE `query_action_url` indexed"

    c.execute("create index if not exists `term_query_term_id_index` on term_query(term_id)")
    c.execute("create index if not exists `term_query_query_id_index` on term_query(query_id)")
    conn.commit()
    print "TABLE `term_query` indexed"

    c.execute("create index if not exists `click_session_id_index` on click(session_id)")
    c.execute("create index if not exists `click_serpid_index` on click(serpid)")
    c.execute("create index if not exists `click_url_id_index` on click(url_id)")
    c.execute("create index if not exists `click_time_passed_index` on click(time_passed)")
    conn.commit()
    print "TABLE `click` indexed"

    conn.commit()
    conn.close()
