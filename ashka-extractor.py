#!/usr/bin/env python
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

import sqlite3
import os
import gzip
import sys
import getopt
import re

#Session metadata (TypeOfRecord = M):
#SessionID TypeOfRecord Day USERID
#Query action (TypeOfRecord = Q or T):
#SessionID TimePassed TypeOfRecord SERPID QueryID ListOfTerms ListOfURLsAndDomains
#Click action (TypeOfRecord = C):
#SessionID TimePassed TypeOfRecord SERPID URLID

def initDB(cursor):
    cursor.execute("create table session ( \
            id integer primary key not null, \
            day integer, \
            user_id integer)")

    cursor.execute("create table url ( \
            id integer primary key not null, \
            domain_id integer)")

    cursor.execute("create table query_action ( \
            id integer primary key autoincrement, \
            session_id integer, \
            serpid integer, \
            query_id integer, \
            time_passed integer, \
            is_test boolean, \
            foreign key(session_id) references session(id))")

    cursor.execute("create table query_action_url ( \
            id integer primary key autoincrement, \
            query_action_id integer, \
            url_id integer, \
            number_in_search_result integer, \
            foreign key(query_action_id) references query_action(id), \
            foreign key(url_id) references url(id))")

    cursor.execute("create table term_query ( \
            id integer primary key autoincrement, \
            term_id integer, \
            query_id integer)")

    cursor.execute("create table click ( \
            id integer primary key autoincrement, \
            session_id integer, \
            serpid integer, \
            url_id integer, \
            time_passed integer, \
            foreign key(session_id) references session(id), \
            foreign key(url_id) references url(id))")


def processTerms(cursor, terms_string, query_id):
    for term_id in terms_string.split(","):
        cursor.execute("insert into `term_query` (`term_id`, `query_id`) values (?, ?)", (term_id,  query_id))

def insertUrl(cursor, url_id, domain_id):
    cursor.execute("insert or replace into `url` values (?, ?)", (url_id, domain_id))

def processListOfUrlsAndDomains(cursor, lst, query_action_id):
    idx = 1
    for pair in [x.split(",") for x in lst]:
        insertUrl(cursor, pair[0], pair[1])
        cursor.execute("insert into `query_action_url` (`query_action_id`, `url_id`, `number_in_search_result`) \
                values (?, ?, ?)",  (query_action_id, pair[0], idx))
        idx += 1

def insertSession(cursor, session_id, label, day, user_id):
    cursor.execute("insert into `session` values (?, ?, ?)", (session_id, day, user_id))

def insertQuery(cursor, is_test_query, session_id, time_passed, type_of_record, serpid, query_id, list_of_terms, *list_of_urls_and_domains):
    cursor.execute("insert into `query_action` (`session_id`, `serpid`, `query_id`, `time_passed`, `is_test`) \
        values (?, ?, ?, ?, ?)", (session_id, serpid, query_id, time_passed, is_test_query))
    query_action_id = cursor.lastrowid

    processTerms(cursor, list_of_terms, query_id)

    processListOfUrlsAndDomains(cursor, list_of_urls_and_domains, query_action_id)

def insertClick(cursor, session_id, time_passed, type_of_record, serpid, url_id):
    cursor.execute("insert into `click` (`session_id`, `serpid`, `url_id`, `time_passed`) \
        values (?, ?, ?, ?)", (session_id, serpid, url_id, time_passed))


if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:], "vwhi:o:m:c:", ["help", "ifile=", "ofile=", "max-lines=", "chunk-size=", "overwrite", "verbose"])
    except getopt.GetoptError as err:
        print str(err)
        print "Usage: " + sys.argv[0] +' -i <inputfile> -o <outputfile> -m <max-lines> -c <chunk-size>'
        sys.exit(2)

    inputfile = "./train.gz"
    outputfile = "./dataset.db"
    max_lines =  float("inf")
    chunk_size =  50000
    overwrite = False
    verbose = False
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print "Usage: " + sys.argv[0] +' -i <inputfile> -o <outputfile> -m <max-lines> -c <chunk-size>'
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        elif opt in ("-m", "--max-lines"):
            max_lines = int(arg)
        elif opt in ("-c", "--chunk-size"):
            chunk_size = int(arg)
        elif opt in ("-w", "--overwrite"):
            overwrite = True
        elif opt in ("-v", "--verbose"):
            verbose = True
        else:
            assert False, "unhandled option"

    if os.path.exists(outputfile):
        if (overwrite):
            os.remove(outputfile)
        else:
            print "Output file '%s' exists. Use '--overwrite' to overwrite it" % outputfile
            sys.exit(2)

    if not os.path.exists(inputfile):
        print "Input file '%s' does not exist!'" % inputfile
        sys.exit(2)

    conn = sqlite3.connect(outputfile, isolation_level = "EXCLUSIVE")

    c = conn.cursor()
    initDB(c)
    conn.commit()

    f = gzip.open(inputfile) if re.match(".*\\.gz$", inputfile) else open(inputfile)

    lines = 0
    for line in f:
        data = line.rstrip().split("\t")
        if len(data) <= 3:
            continue

        if data[1] == "M":
            insertSession(c, *data)

        if data[2] == "T" or data[2] == "Q":
            insertQuery(c, data[2] == "T", *data)

        if data[2] == "C":
            insertClick(c, *data)

        lines += 1
        if lines >= max_lines:
            break
        if lines % chunk_size == 0:
            conn.commit()
            if verbose:
                print "%d lines processed" % lines

    print "%d lines processed" % lines
    conn.commit()
    conn.close()
