library(sqldf)


countEtropy <- function(total, sub_dataset) {
  -(sum(sapply(sub_dataset, function(e) {
    p <- e / total 
    p * log2(p)
  })))
}



countEntropies <- function(dataset) {
  
  n <- nrow(dataset)
  id <- dataset[1, "user_id"]
  start_idx <- 1
  entrop <- 0
  total <- 0
  res <- vector()
  
  for (i in 1:n) {
      if (id != dataset[i, "user_id"]) {
          if (total > 0) {
            end_idx <- i - 1
          	entrop = countEtropy(total, dataset[start_idx:end_idx, "items_count"])
            
            res <- append(res, entrop / total)
          	total <- 0
  	      } 
          
          start_idx <- i
          id <- dataset[i, "user_id"]
      } 
      
      total <- total + dataset[i, "items_count"]
  }
  
  res
}

drawPlot <- function() {
  db_str <- "/Users/Katerina/Documents/gamechangers/dmlabs/repo2/kaggle-yandex/data/dataset.db"
  dataset <- sqldf("select user_id, query_id, count(query_id) as items_count from session inner join query_action on session.id = query_action.session_id group by user_id, query_id order by user_id", dbname=db_str)
  
  res <- countEntropies(dataset)
  qplot(res, geom="histogram") 
}

